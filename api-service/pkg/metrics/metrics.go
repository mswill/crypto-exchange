package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

var (
	// HTTP request metrics
	httpRequestsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "_api_http_requests_total",
			Help: "Total number of HTTP requests",
		},
		[]string{"method", "endpoint", "status"},
	)

	httpRequestDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "_api_http_request_duration_seconds",
			Help:    "Duration of HTTP requests in seconds",
			Buckets: prometheus.DefBuckets,
		},
		[]string{"method", "endpoint"},
	)

	// Database metrics
	dbQueryDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "_api_db_query_duration_seconds",
			Help:    "Duration of database queries in seconds",
			Buckets: prometheus.DefBuckets,
		},
		[]string{"query"},
	)

	dbQueryErrorsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "_api_db_query_errors_total",
			Help: "Total number of database query errors",
		},
		[]string{"query"},
	)

	// Redis metrics
	redisOperationDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "_api_redis_operation_duration_seconds",
			Help:    "Duration of Redis operations in seconds",
			Buckets: prometheus.DefBuckets,
		},
		[]string{"operation"},
	)

	redisOperationErrorsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "_api_redis_operation_errors_total",
			Help: "Total number of Redis operation errors",
		},
		[]string{"operation"},
	)
)

func init() {
	prometheus.MustRegister(httpRequestsTotal)
	prometheus.MustRegister(httpRequestDuration)
	prometheus.MustRegister(dbQueryDuration)
	prometheus.MustRegister(dbQueryErrorsTotal)
	prometheus.MustRegister(redisOperationDuration)
	prometheus.MustRegister(redisOperationErrorsTotal)
}

func MetricsHandler() http.Handler {
	return promhttp.Handler()
}

func IncrementHTTPRequestsTotal(method, endpoint, status string) {
	httpRequestsTotal.WithLabelValues(method, endpoint, status).Inc()
}

func ObserveHTTPRequestDuration(method, endpoint string, duration float64) {
	httpRequestDuration.WithLabelValues(method, endpoint).Observe(duration)
}

func ObserveDBQueryDuration(query string, duration float64) {
	dbQueryDuration.WithLabelValues(query).Observe(duration)
}

func IncrementDBQueryErrorsTotal(query string) {
	dbQueryErrorsTotal.WithLabelValues(query).Inc()
}

func ObserveRedisOperationDuration(operation string, duration float64) {
	redisOperationDuration.WithLabelValues(operation).Observe(duration)
}

func IncrementRedisOperationErrorsTotal(operation string) {
	redisOperationErrorsTotal.WithLabelValues(operation).Inc()
}
