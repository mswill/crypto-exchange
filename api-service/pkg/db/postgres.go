package db

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
	"os"
)

func Connect() (*pgxpool.Pool, error) {
	//dsn := fmt.Sprintf("postgresql://%s:%s@%s:%d/%s",
	//	viper.GetString("database.user"),
	//	viper.GetString("database.password"),
	//	viper.GetString("database.host"),
	//	viper.GetInt("database.port"),
	//	viper.GetString("database.name"),
	//)
	dsn := os.Getenv("DATABASE_URL")
	fmt.Println("dsn postgres ", dsn)

	config, err := pgxpool.ParseConfig(dsn)
	if err != nil {
		fmt.Println("Error parsing config:", err)
		return nil, fmt.Errorf("unable to parse database config: %w", err)
	}

	pool, err := pgxpool.NewWithConfig(context.Background(), config)
	if err != nil {
		fmt.Println("Error creating pool:", err)
		return nil, fmt.Errorf("unable to create database pool: %w", err)
	}
	fmt.Println("-----------------------------")
	fmt.Println()
	fmt.Println("-----------------------------")

	return pool, nil
}

func Close(pool *pgxpool.Pool) {
	pool.Close()
}
