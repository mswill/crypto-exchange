package redis

import (
	"context"
	"fmt"
	"github.com/redis/go-redis/v9"
)

var ctx = context.Background()

type RedisClient struct {
	client *redis.Client
}

func NewRedisClient(addr, password string, db int) *RedisClient {
	rdb := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: password,
		DB:       db,
	})
	fmt.Println()
	fmt.Println("------- redis client ")
	fmt.Println("------- addr ", addr)
	fmt.Println("------- password ", password)
	fmt.Println("------- db ", db)
	fmt.Println()

	return &RedisClient{client: rdb}
}

func (r *RedisClient) Set(key string, value interface{}) error {
	return r.client.Set(ctx, key, value, 0).Err()
}

func (r *RedisClient) Get(key string) (string, error) {
	return r.client.Get(ctx, key).Result()
}

func (r *RedisClient) Close() error {
	return r.client.Close()
}
