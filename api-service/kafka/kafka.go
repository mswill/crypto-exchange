package kafka

import (
	"context"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"github.com/segmentio/kafka-go"
	"gitlab.com/mswill/api-service/internal/usecase"
)

type KafkaConsumer struct {
	reader  *kafka.Reader
	usecase *usecase.UseCase
}

func NewKafkaConsumer(brokers []string, topic, groupID string, uc *usecase.UseCase) *KafkaConsumer {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers: brokers,
		Topic:   topic,
		GroupID: groupID,
	})
	return &KafkaConsumer{
		reader:  reader,
		usecase: uc,
	}
}

func (kc *KafkaConsumer) Start(ctx context.Context) {
	for {
		m, err := kc.reader.ReadMessage(ctx)
		if err != nil {
			log.Error().Err(err).Msg("Failed to read message from Kafka")
			continue
		}

		var message map[string]interface{}
		if err := json.Unmarshal(m.Value, &message); err != nil {
			log.Error().Err(err).Msg("Failed to unmarshal Kafka message")
			continue
		}

		event := message["event"].(string)
		if event == "rates_updated" || event == "crypto_rates_updated" {
			log.Info().Msg("Received rates updated event, updating cache")
			if err := kc.usecase.UpdateCache(ctx); err != nil {
				log.Error().Err(err).Msg("Failed to update cache")
			}
		}
	}
}

func (kc *KafkaConsumer) Close() error {
	return kc.reader.Close()
}
