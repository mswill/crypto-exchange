package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mswill/api-service/internal/usecase"
	"gitlab.com/mswill/api-service/pkg/metrics"
	"gitlab.com/mswill/api-service/pkg/tracing"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"strconv"
	"strings"
	"time"
)

type Controller struct {
	usecase *usecase.UseCase
	tr      trace.Tracer
}

func NewController(uc *usecase.UseCase, tr trace.Tracer) *Controller {
	return &Controller{usecase: uc, tr: tr}
}

// GetCurrencies Get all currencies
// @Summary Get all currencies
// @Description Get all available currencies
// @ID  get_currencies_get
// @Tags all currencies
// @Accept  json
// @Produce  json
// @Success 200 {array} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Router /currencies [get]
func (c *Controller) GetCurrencies(ctx *fiber.Ctx) error {
	ctxx, span := tracing.StartSpan(ctx.Context(), c.tr, "/currencies")
	defer span.End()
	start := time.Now()
	currencies, err := c.usecase.GetCurrencies(ctxx)
	if err != nil {
		return ctx.
			Status(fiber.StatusInternalServerError).
			JSON(fiber.Map{"error": err.Error()})
	}

	duration := time.Since(start).Seconds()
	metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
	metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "200")

	return ctx.JSON(fiber.Map{
		"result": currencies,
		"time_spent": fiber.Map{
			"ms":           time.Since(start).Milliseconds(),
			"microseconds": time.Since(start).Microseconds(),
			"nanoseconds":  time.Since(start).Nanoseconds(),
		},
	})
}

// ConvertCurrency godoc
// @Summary Convert currency
// @Description Convert amount from one currency to another
// @Tags exchange
// @Accept  json
// @Produce  json
// @Param from query string true "From currency code"
// @Param to query string true "To currency code"
// @Param amount query float64 true "Amount to convert"
// @Success 200 {object} map[string]interface{}
// @Failure 400 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Router /convert [get]
func (c *Controller) ConvertCurrency(ctx *fiber.Ctx) error {
	ctxx, span := tracing.StartSpan(ctx.Context(), c.tr, "/convert [get]")
	defer span.End()

	start := time.Now()
	from := strings.ToUpper(ctx.Query("from"))
	to := strings.ToUpper(ctx.Query("to"))
	amount, err := strconv.ParseFloat(ctx.Query("amount"), 64)
	if err != nil {
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "400")
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{"error": "Invalid amount"})
	}
	span.SetAttributes(
		attribute.StringSlice("exchange", []string{from, to}),
		attribute.Int64("amount", int64(amount)),
	)
	result, err := c.usecase.ConvertCurrency(ctxx, from, to, amount)
	if err != nil {
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "500")
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}
	span.SetAttributes(
		attribute.Int64("result", int64(result)),
	)

	duration := time.Since(start).Seconds()
	metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
	metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "200")
	return ctx.JSON(fiber.Map{
		"result": result,
		"time_spent": fiber.Map{
			"ms":           time.Since(start).Milliseconds(),
			"microseconds": time.Since(start).Microseconds(),
			"nanoseconds":  time.Since(start).Nanoseconds(),
		},
	})
}
func (c *Controller) Check(ctx *fiber.Ctx) error {
	return ctx.SendStatus(fiber.StatusOK)
}
