package usecase

import (
	"context"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/mswill/api-service/internal/model"
	"gitlab.com/mswill/api-service/internal/repository"
	"gitlab.com/mswill/api-service/pkg/metrics"
	"gitlab.com/mswill/api-service/pkg/redis"
	"gitlab.com/mswill/api-service/pkg/tracing"
	"go.opentelemetry.io/otel/trace"
	"strconv"
	"time"
)

type Repository interface {
	GetCurrencies(ctx context.Context) ([]model.Currency, error)
	GetAvailableCurrencies(ctx context.Context) ([]model.Currency, []model.Currency, error)
	GetRates(baseCurrency string) (map[string]float64, error)
	GetCryptoRates(baseCurrency string) (map[string]float64, error)
	IsCurrencyAvailable(ctx context.Context, code string) (bool, error)
	IsCryptoCurrencyAvailable(ctx context.Context, code string) (bool, error)
}
type UseCase struct {
	repo  Repository
	cache *redis.RedisClient
	tr    trace.Tracer
}

func NewUseCase(repo *repository.Repository, cache *redis.RedisClient, tr trace.Tracer) *UseCase {
	return &UseCase{repo: repo, cache: cache, tr: tr}
}

func (uc *UseCase) GetCurrencies(ctx context.Context) ([]model.Currency, error) {
	ctxx, span := tracing.StartSpan(ctx, uc.tr, "UseCase.GetCurrencies")
	defer span.End()
	start := time.Now()

	currencies, err := uc.repo.GetCurrencies(ctxx)
	duration := time.Since(start).Seconds()
	metrics.ObserveDBQueryDuration("GetCurrencies", duration)

	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("GetCurrencies")
	}

	return currencies, err
}

func (uc *UseCase) UpdateCache(ctx context.Context) error {

	currencies, cryptoCurrencies, err := uc.repo.GetAvailableCurrencies(ctx)
	if err != nil {
		return err
	}

	// Update regular currency rates
	for _, currency := range currencies {
		rates, err := uc.repo.GetRates(currency.Code)
		if err != nil {
			log.Error().Err(err).Msg("Failed to get rates for currency")
			continue
		}
		for target, rate := range rates {
			key := currency.Code + ":" + target
			if err := uc.cache.Set(key, fmt.Sprintf("%.8f", rate)); err != nil {
				log.Error().Err(err).Msg("Failed to set cache for currency rate")
			}

			// Cache reverse rate
			reverseKey := target + ":" + currency.Code

			reverseRate := 1 / rate
			if err := uc.cache.Set(reverseKey, fmt.Sprintf("%.8f", reverseRate)); err != nil {
				log.Error().Err(err).Msg("Failed to set cache for reverse currency rate")
			}
		}
	}

	// Update crypto currency rates
	for _, currency := range cryptoCurrencies {
		rates, err := uc.repo.GetCryptoRates(currency.Code)
		if err != nil {
			log.Error().Err(err).Msg("Failed to get crypto rates for currency")
			continue
		}
		for target, rate := range rates {
			key := target + ":" + currency.Code
			if err := uc.cache.Set(key, fmt.Sprintf("%.8f", rate)); err != nil {
				log.Error().Err(err).Msg("Failed to set cache for crypto currency rate")
			}

			// Cache reverse rate
			reverseKey := currency.Code + ":" + target
			reverseRate := 1 / rate
			if err := uc.cache.Set(reverseKey, fmt.Sprintf("%.8f", reverseRate)); err != nil {
				log.Error().Err(err).Msg("Failed to set cache for reverse crypto currency rate")
			}
		}
	}

	return nil
}

func (uc *UseCase) ConvertCurrency(ctx context.Context, base, target string, amount float64) (float64, error) {
	ctxx, span := tracing.StartSpan(ctx, uc.tr, "Usecase.ConvertCurrency")
	defer span.End()
	start := time.Now()

	// Проверка доступности валюты
	isBaseAvailable, err := uc.repo.IsCurrencyAvailable(ctxx, base)
	if err != nil && err.Error() != "no rows in result set" {
		metrics.IncrementDBQueryErrorsTotal("IsCurrencyAvailable")
		return 0, err
	}
	if err == nil && !isBaseAvailable {
		return 0, fmt.Errorf("%s is not available for exchange", base)
	}

	isTargetAvailable, err := uc.repo.IsCurrencyAvailable(ctxx, target)
	if err != nil && err.Error() != "no rows in result set" {
		metrics.IncrementDBQueryErrorsTotal("IsCurrencyAvailable")
		return 0, err
	}
	if err == nil && !isTargetAvailable {
		return 0, fmt.Errorf("%s is not available for exchange", target)
	}

	if err != nil && err.Error() == "no rows in result set" {
		isBaseCryptoAvailable, err := uc.repo.IsCryptoCurrencyAvailable(ctxx, base)
		if err != nil {
			metrics.IncrementDBQueryErrorsTotal("IsCryptoCurrencyAvailable")
			return 0, err
		}
		if !isBaseCryptoAvailable {
			return 0, fmt.Errorf("%s is not available for exchange", base)
		}

		isTargetCryptoAvailable, err := uc.repo.IsCryptoCurrencyAvailable(ctxx, target)
		if err != nil {
			metrics.IncrementDBQueryErrorsTotal("IsCryptoCurrencyAvailable")
			return 0, err
		}
		if !isTargetCryptoAvailable {
			return 0, fmt.Errorf("%s is not available for exchange", target)
		}
	}

	key := fmt.Sprintf("%s:%s", base, target)
	rateStr, err := uc.cache.Get(key)
	if err != nil {
		metrics.IncrementRedisOperationErrorsTotal("Get")
		log.Error().Err(err).Msg("Failed to get rate from Redis")
		return 0, err
	}

	rate, err := strconv.ParseFloat(rateStr, 64)
	if err != nil {
		metrics.IncrementRedisOperationErrorsTotal("ParseFloat")
		log.Error().Err(err).Msg("Failed to parse rate")
		return 0, err
	}

	duration := time.Since(start).Seconds()
	metrics.ObserveRedisOperationDuration("Get", duration)

	return amount * rate, nil
}
