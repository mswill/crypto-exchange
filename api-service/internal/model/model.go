package model

type Currency struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Code      string `json:"code"`
	Available bool   `json:"available"`
}
