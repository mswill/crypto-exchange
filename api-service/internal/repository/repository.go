package repository

import (
	"context"
	s "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/mswill/api-service/internal/model"
	"gitlab.com/mswill/api-service/pkg/tracing"
	"go.opentelemetry.io/otel/trace"
	"sync"
)

var (
	currencies       = "currencies"
	currencyRates    = "currency_rates"
	cryptoCurrencies = "crypto_currencies"
)

type Repository struct {
	db *pgxpool.Pool
	s  s.StatementBuilderType
	tr trace.Tracer
}

func NewRepository(db *pgxpool.Pool, tr trace.Tracer) *Repository {
	return &Repository{
		db: db,
		s:  s.StatementBuilder.PlaceholderFormat(s.Dollar),
		tr: tr,
	}
}

func (r *Repository) GetCurrencies(ctx context.Context) ([]model.Currency, error) {
	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository.GetCurrencies")
	defer span.End()

	rows, err := r.db.Query(ctxx, "SELECT id, name, code, available FROM currencies UNION SELECT id, name, code, available FROM crypto_currencies")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var currencies []model.Currency
	for rows.Next() {
		var currency model.Currency
		if err := rows.Scan(&currency.ID, &currency.Name, &currency.Code, &currency.Available); err != nil {
			return nil, err
		}
		currencies = append(currencies, currency)
	}

	return currencies, nil
}

// GetAvailableCurrencies Получение доступных валют и криптовалют
func (r *Repository) GetAvailableCurrencies(ctx context.Context) ([]model.Currency, []model.Currency, error) {
	span := trace.SpanFromContext(ctx)
	defer span.End()

	var wg sync.WaitGroup
	currencyChan := make(chan []model.Currency)
	cryptoCurrencyChan := make(chan []model.Currency)
	errorChan := make(chan error, 2)

	wg.Add(2)

	// Горутина для получения доступных обычных валют
	go func() {
		defer wg.Done()
		var currencies []model.Currency

		rows, err := r.db.Query(context.Background(), "SELECT id, name, code, available FROM currencies WHERE available = true")
		if err != nil {
			errorChan <- err
			return
		}
		defer rows.Close()

		for rows.Next() {
			var currency model.Currency
			if err := rows.Scan(&currency.ID, &currency.Name, &currency.Code, &currency.Available); err != nil {
				errorChan <- err
				return
			}
			currencies = append(currencies, currency)
		}
		currencyChan <- currencies
	}()

	// Горутина для получения доступных криптовалют
	go func() {
		defer wg.Done()
		var cryptoCurrencies []model.Currency

		rows, err := r.db.Query(context.Background(), "SELECT id, name, code, available FROM crypto_currencies WHERE available = true")
		if err != nil {
			errorChan <- err
			return
		}
		defer rows.Close()

		for rows.Next() {
			var currency model.Currency
			if err := rows.Scan(&currency.ID, &currency.Name, &currency.Code, &currency.Available); err != nil {
				errorChan <- err
				return
			}
			cryptoCurrencies = append(cryptoCurrencies, currency)
		}
		cryptoCurrencyChan <- cryptoCurrencies
	}()

	go func() {
		wg.Wait()
		close(currencyChan)
		close(cryptoCurrencyChan)
		close(errorChan)
	}()

	var currencies []model.Currency
	var cryptoCurrencies []model.Currency
	var err error

	// Ожидание результатов обеих горутин или ошибки
	for i := 0; i < 2; i++ {
		select {
		case curr := <-currencyChan:
			currencies = curr
		case cryptoCurr := <-cryptoCurrencyChan:
			cryptoCurrencies = cryptoCurr
		case e := <-errorChan:
			err = e
		}
	}

	if err != nil {
		return nil, nil, err
	}

	return currencies, cryptoCurrencies, nil
}

// GetRates Получение курсов обычных валют
func (r *Repository) GetRates(baseCurrency string) (map[string]float64, error) {
	rows, err := r.db.Query(context.Background(), "SELECT target_currency_id, rate FROM currency_rates WHERE base_currency_id = (SELECT id FROM currencies WHERE code = $1)", baseCurrency)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	rates := make(map[string]float64)
	for rows.Next() {
		var targetID int
		var rate int64
		if err := rows.Scan(&targetID, &rate); err != nil {
			return nil, err
		}
		var targetCode string
		err = r.db.QueryRow(context.Background(), "SELECT code FROM currencies WHERE id = $1", targetID).Scan(&targetCode)
		if err != nil {
			return nil, err
		}
		rates[targetCode] = float64(rate) / 100000000
	}
	return rates, nil
}

// GetCryptoRates Получение курсов криптовалют
func (r *Repository) GetCryptoRates(baseCurrency string) (map[string]float64, error) {
	rows, err := r.db.Query(context.Background(), "SELECT target_currency_id, rate FROM crypto_currency_rates WHERE base_currency_id = (SELECT id FROM crypto_currencies WHERE code = $1)", baseCurrency)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	rates := make(map[string]float64)
	for rows.Next() {
		var targetID int
		var rate int64
		if err := rows.Scan(&targetID, &rate); err != nil {
			return nil, err
		}
		var targetCode string
		err = r.db.QueryRow(context.Background(), "SELECT code FROM crypto_currencies WHERE id = $1", targetID).Scan(&targetCode)
		if err != nil {
			return nil, err
		}
		rates[targetCode] = float64(rate) / 100000000
	}
	return rates, nil
}

func (r *Repository) IsCurrencyAvailable(ctx context.Context, code string) (bool, error) {
	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository.IsCurrencyAvailable")
	defer span.End()

	var available bool
	err := r.db.QueryRow(ctxx, "SELECT available FROM currencies WHERE code=$1", code).Scan(&available)
	if err != nil {
		return false, err
	}
	return available, nil
}

func (r *Repository) IsCryptoCurrencyAvailable(ctx context.Context, code string) (bool, error) {
	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository.IsCryptoCurrencyAvailable")
	defer span.End()

	var available bool
	err := r.db.QueryRow(ctxx, "SELECT available FROM crypto_currencies WHERE code=$1", code).Scan(&available)
	if err != nil {
		return false, err
	}
	return available, nil
}
