package server

import (
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"gitlab.com/mswill/api-service/internal/controllers"
	"os"
	"os/signal"
	"syscall"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

// StartServer инициализирует и запускает Fiber сервер
func StartServer(app *fiber.App, ctrl *controllers.Controller) {

	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept, Content-Length, Accept-Language, Accept-Encoding, Connection, Access-Control-Allow-Origin",
		AllowMethods: "GET,POST,HEAD,PUT,DELETE,PATCH,OPTIONS",
	}))

	// logger
	app.Use(logger.New())

	// Routes setup
	app.Get("/convert", ctrl.ConvertCurrency)
	app.Get("/currencies", ctrl.GetCurrencies)
	app.Get("/health", ctrl.Check)

	serverAddress := viper.GetString("server.address")
	serverPort := viper.GetString("server.port")
	// Start server
	go func() {
		if err := app.Listen(serverAddress + ":" + serverPort); err != nil {
			log.Fatal().Err(err).Msg("Failed to start server")
		}
	}()

	log.Info().Msg("Exchange Service started")

	// Graceful shutdown
	shutdownChan := make(chan os.Signal, 1)
	signal.Notify(shutdownChan, syscall.SIGTERM, syscall.SIGINT)
	<-shutdownChan

	log.Info().Msg("Shutting down gracefully...")

	if err := app.Shutdown(); err != nil {
		log.Fatal().Err(err).Msg("Failed to gracefully shut down server")
	}

	log.Info().Msg("Server stopped")
}
