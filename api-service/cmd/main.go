package main

import (
	"context"
	"fmt"
	"gitlab.com/mswill/api-service/pkg/consul"
	"gitlab.com/mswill/api-service/pkg/metrics"
	"gitlab.com/mswill/api-service/pkg/tracing"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/swagger"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"

	_ "gitlab.com/mswill/api-service/docs"
	"gitlab.com/mswill/api-service/internal/controllers"
	"gitlab.com/mswill/api-service/internal/repository"
	"gitlab.com/mswill/api-service/internal/server"
	"gitlab.com/mswill/api-service/internal/usecase"
	"gitlab.com/mswill/api-service/kafka"
	"gitlab.com/mswill/api-service/pkg/db"
	"gitlab.com/mswill/api-service/pkg/redis"
)

func initConfig() {
	viper.AddConfigPath(".")
	viper.AddConfigPath("./config")
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal().Err(err).Msg("Error reading config file")
	}
}

const serviceName = "Exchange Service"
const endpoint = "http://jaeger:14268/api/traces"

// @title Сервис обмена валют Exchange Service API
// @version 1.0
// @description Обрабатывает запросы на конвертацию валют.
// @description Кэширует курсы валют в Redis для быстрого доступа.
// @description Подписывается на сообщения из Kafka для обновления кэша.
// @description Проверяет доступность валют перед выполнением конвертации.
// @host localhost:12000
// @BasePath /
func main() {
	initConfig()

	// Initialize tracing
	tracer, traceFunc := tracing.InitTracer(serviceName, endpoint)
	defer traceFunc()

	// Initialize logger
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	// Initialize database connection
	dbConn, err := db.Connect()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to connect to database")
	}
	defer db.Close(dbConn)

	// Initialize Redis connection
	redisClient := redis.NewRedisClient(
		viper.GetString("redis.addr"),
		viper.GetString("redis.password"),
		viper.GetInt("redis.db"),
	)

	fmt.Println()
	fmt.Println("REDIS_ADDR ", os.Getenv("REDIS_ADDR"))
	fmt.Println("redis.password ", viper.GetString("redis.password"))
	fmt.Println("redis.db ", viper.GetInt("redis.db"))
	fmt.Println()

	// Initialize Kafka consumer
	kafkaBrokers := []string{os.Getenv("KAFKA_ADDR_0"), os.Getenv("KAFKA_ADDR_1")}
	kafkaTopic := viper.GetString("kafka.topic")
	kafkaGroupID := viper.GetString("kafka.group_id")

	// Initialize repository and usecase
	repo := repository.NewRepository(dbConn, tracer)
	usecase := usecase.NewUseCase(repo, redisClient, tracer)
	ctrl := controllers.NewController(usecase, tracer)

	// Initialize Consul client
	consulClient, err := consul.NewConsulClient("consul:8500")
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to create Consul client")
	}

	// Register service with Consul
	err = consulClient.RegisterService(serviceName, 12000)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to register service with Consul")
	}

	// Initialize Fiber app
	app := fiber.New()
	// Swagger setup
	app.Get("/swagger/*", swagger.HandlerDefault)

	// Metrics endpoint
	go func() {
		serverAddress := viper.GetString("server.address")
		metricsServer := &http.Server{
			Addr:    serverAddress + ":2112",
			Handler: metrics.MetricsHandler(),
		}
		log.Info().Msg("Starting metrics server at :2112")
		if err := metricsServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal().Err(err).Msg("Failed to start metrics server")
		}
	}()

	// Start server
	go server.StartServer(app, ctrl)

	// Start Kafka consumer
	kafkaConsumer := kafka.NewKafkaConsumer(kafkaBrokers, kafkaTopic, kafkaGroupID, usecase)
	go kafkaConsumer.Start(context.Background())

	// Graceful shutdown
	shutdownChan := make(chan os.Signal, 1)
	signal.Notify(shutdownChan, syscall.SIGTERM, syscall.SIGINT)

	// Listen for interrupt
	<-shutdownChan

	// Graceful shutdown
	log.Info().Msg("Shutting down gracefully...")

	if err := kafkaConsumer.Close(); err != nil {
		log.Error().Err(err).Msg("Failed to close Kafka consumer")
	} else {
		log.Info().Msg("Kafka consumer closed successfully")
	}

	if err := redisClient.Close(); err != nil {
		log.Error().Err(err).Msg("Failed to close Redis client")
	} else {
		log.Info().Msg("Redis client closed successfully")
	}

	db.Close(dbConn)
	log.Info().Msg("Postgres database closed successfully")

	if err := app.Shutdown(); err != nil {
		log.Error().Err(err).Msg("Failed to gracefully shut down server")
	} else {
		log.Info().Msg("Fiber server stopped")
	}
}
