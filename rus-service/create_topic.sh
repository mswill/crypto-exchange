#!/bin/bash

/opt/bitnami/kafka/bin/kafka-topics.sh --create --if-not-exists --topic 'rates_updated' --bootstrap-server kafka0:9092 --replication-factor 1 --partitions 1
echo "topic $TEST_TOPIC_NAME was created"
