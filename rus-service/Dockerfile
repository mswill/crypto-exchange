# Используем правильный базовый образ для сборки Go
FROM golang:1.21-alpine AS builder

RUN echo '----------------- DEPLOY Rate Updater Service -----------------'
WORKDIR /app

# Установим зависимости для сборки
RUN apk add --no-cache make gcc libc-dev

# Устанавливаем утилиту goose
RUN go install github.com/pressly/goose/v3/cmd/goose@latest

# Копируем файлы для сборки
COPY go.mod ./
COPY go.sum ./

RUN go mod download

COPY . .

# Собираем бинарник
RUN CGO_ENABLED=0 GOOS=linux go build -o updater ./cmd/main.go

RUN echo '----------------- BUILD Rate Updater Service -----------------'

# Используем минимальный образ для запуска
FROM alpine

WORKDIR /app

# Копируем скомпилированный бинарник из предыдущего этапа
COPY --from=builder /app/updater /app/updater

# Копируем конфигурационный файл
COPY --from=builder /app/config/config.yaml /app/config/config.yaml

# Устанавливаем точку входа
ENTRYPOINT [ "/app/updater" ]
