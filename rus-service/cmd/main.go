package main

import (
	"context"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mswill/updater-service/pkg/consul"
	"gitlab.com/mswill/updater-service/pkg/metrics"
	"gitlab.com/mswill/updater-service/pkg/tracing"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"

	"gitlab.com/mswill/updater-service/internal/repository"
	"gitlab.com/mswill/updater-service/internal/usecase"
	"gitlab.com/mswill/updater-service/kafka"
	"gitlab.com/mswill/updater-service/pkg/db"
	"gitlab.com/mswill/updater-service/pkg/external"
)

func initConfig() {
	viper.AddConfigPath(".")
	viper.AddConfigPath("./config")
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal().Err(err).Msg("Error reading config file")
	}
}

const serviceName = "Rate Updater Service"
const endpoint = "http://jaeger:14268/api/traces"

// @title Updater Service API
// @version 1.0
// @description This is the API documentation for the Updater Service.
// @host localhost:10000
// @BasePath /
func main() {
	initConfig()
	ctx := context.Background()
	// Initialize tracing
	tracer, traceFunc := tracing.InitTracer(serviceName, endpoint)
	defer traceFunc()

	// Initialize Fiber app
	app := fiber.New()

	// Initialize Consul client
	consulClient, err := consul.NewConsulClient("consul:8500")
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to create Consul client")
	}

	// Register service with Consul
	err = consulClient.RegisterService(serviceName, 10000)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to register service with Consul")
	}

	// Initialize logger
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	// Initialize database connection
	dbConn, err := db.Connect()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to connect to database")
	}
	defer db.Close(dbConn)

	// Initialize external API client
	apiClient := external.NewFastForexClient(
		viper.GetString("fastforex.api_key"),
		viper.GetString("fastforex.url"),
		tracer,
	)

	// Получаем список брокеров и тему из конфигурации
	kafkaBrokers := viper.GetStringSlice("kafka.brokers")
	kafkaTopic := viper.GetString("kafka.topic")
	fmt.Println()
	fmt.Println("KAFKA SETUP ", kafkaBrokers, " ", kafkaTopic)
	fmt.Println()

	// Инициализация Kafka producer
	kafkaProducer := kafka.NewKafkaProducer(kafkaBrokers, kafkaTopic, tracer)

	// Initialize repository and usecase
	repo := repository.NewRepository(dbConn, tracer)
	usecase := usecase.NewUseCase(repo, apiClient, kafkaProducer, tracer)

	serverAddress := viper.GetString("server.address")
	serverPort := viper.GetString("server.port")

	// Start Fiber server
	go func() {
		if err := app.Listen(serverAddress + ":" + serverPort); err != nil {
			log.Fatal().Err(err).Msg("Failed to start server")
		}
	}()

	// Metrics endpoint
	go func() {
		metricsServer := &http.Server{
			Addr:    ":2114",
			Handler: metrics.MetricsHandler(),
		}
		log.Info().Msg("Starting metrics server at :2114")
		if err := metricsServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal().Err(err).Msg("Failed to start metrics server")
		}
	}()
	app.Get("/health", func(ctx *fiber.Ctx) error {
		return ctx.SendStatus(fiber.StatusOK)
	})
	// Start background rate updater
	go startRateUpdater(ctx, usecase)

	// Graceful shutdown
	shutdownChan := make(chan os.Signal, 1)
	signal.Notify(shutdownChan, syscall.SIGTERM, syscall.SIGINT)

	// Listen for interrupt
	<-shutdownChan

	// Graceful shutdown
	log.Info().Msg("Shutting down gracefully...")

	if err := kafkaProducer.Close(); err != nil {
		log.Error().Err(err).Msg("Failed to close Kafka producer")
	} else {
		log.Info().Msg("Kafka producer closed successfully")
	}

	db.Close(dbConn)
	log.Info().Msg("Postgres database closed successfully")

	if err := app.Shutdown(); err != nil {
		log.Error().Err(err).Msg("Failed to gracefully shut down server")
	} else {
		log.Info().Msg("Fiber server stopped")
	}
}

// ---------- KAFKA -------------
func startRateUpdater(ctx context.Context, uc *usecase.UseCase) {
	ticker := time.NewTicker(time.Second * viper.GetDuration("kafka.tik"))
	defer ticker.Stop()
	fmt.Println()
	fmt.Println(" --- startRateUpdater --- ")
	fmt.Println("kafka update time: ", time.Second*viper.GetDuration("kafka.tik"))
	fmt.Println()

	for {
		select {
		case <-ticker.C:
			// Обновляем обычные валюты
			if err := uc.UpdateRates(ctx); err != nil {
				log.Error().Err(err).Msg("Failed to update")
			} else {
				log.Info().Msg("Successfully updated")
			}

			// Обновляем криптовалюты
			if err := uc.UpdateCryptoRates(ctx); err != nil {
				log.Error().Err(err).Msg("Failed to update crypto rates")
			} else {
				log.Info().Msg("Successfully updated crypto rates")
			}
		}
	}
}
