-- +goose Up
-- +goose StatementBegin
SELECT 'up SQL query';
-- +goose StatementEnd
-- Создание таблицы currencies

CREATE TABLE currencies (
    id BIGINT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    code VARCHAR(10) NOT NULL,
    available BOOLEAN DEFAULT true
);

CREATE TABLE crypto_currencies (
    id BIGINT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    code VARCHAR(10) NOT NULL,
    available BOOLEAN DEFAULT true
);

CREATE TABLE currency_rates (
    id SERIAL PRIMARY KEY,
    base_currency_id INTEGER,
    target_currency_id INTEGER,
    rate BIGINT NOT NULL,
    last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (base_currency_id, target_currency_id),
    FOREIGN KEY (base_currency_id) REFERENCES currencies(id) ON DELETE CASCADE,
    FOREIGN KEY (target_currency_id) REFERENCES currencies(id) ON DELETE CASCADE
);

CREATE TABLE crypto_currency_rates (
    id SERIAL PRIMARY KEY,
    base_currency_id INTEGER,
    target_currency_id INTEGER,
    rate BIGINT NOT NULL,
    last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (base_currency_id, target_currency_id),
    FOREIGN KEY (base_currency_id) REFERENCES crypto_currencies(id) ON DELETE CASCADE,
    FOREIGN KEY (target_currency_id) REFERENCES crypto_currencies(id) ON DELETE CASCADE
);

-- Вставка начальных данных в таблицу currencies
INSERT INTO currencies (id, name, code, available) VALUES
(195736654, 'US Dollar', 'USD', true);

-- Вставка начальных данных в таблицу crypto_currencies
INSERT INTO crypto_currencies (id, name, code, available) VALUES
(495734654, 'Tether', 'USDT', true);


-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';
DROP TABLE IF EXISTS crypto_currency_rates;
DROP TABLE IF EXISTS currency_rates;
DROP TABLE IF EXISTS crypto_currencies;
DROP TABLE IF EXISTS currencies;
-- +goose StatementEnd
