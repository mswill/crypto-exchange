package repository

import (
	"context"
	"fmt"
	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/mswill/updater-service/internal/model"
	"gitlab.com/mswill/updater-service/pkg/metrics"
	"gitlab.com/mswill/updater-service/pkg/tracing"
	"go.opentelemetry.io/otel/trace"

	"strings"
	"sync"
	"time"
)

type Repository struct {
	db *pgxpool.Pool
	s  squirrel.StatementBuilderType
	tr trace.Tracer
}

func NewRepository(db *pgxpool.Pool, tr trace.Tracer) *Repository {
	return &Repository{
		db: db,
		s:  squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar),
		tr: tr,
	}
}

func (r *Repository) GetAvailableCurrencies(ctx context.Context) ([]model.Currency, []model.Currency, error) {

	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository:GetAvailableCurrencies")
	defer span.End()

	start := time.Now()
	var wg sync.WaitGroup
	currencyChan := make(chan []model.Currency)
	cryptoCurrencyChan := make(chan []model.Currency)
	errorChan := make(chan error, 2)

	wg.Add(2)

	// Запуск горутины для получения доступных обычных валют
	go func() {
		defer wg.Done()
		var currencies []model.Currency

		rows, err := r.db.Query(ctxx, "SELECT id, name, code, available FROM currencies WHERE available = true")
		if err != nil {
			errorChan <- err
			return
		}
		defer rows.Close()

		for rows.Next() {
			var currency model.Currency
			if err := rows.Scan(&currency.ID, &currency.Name, &currency.Code, &currency.Available); err != nil {
				errorChan <- err
				return
			}
			currencies = append(currencies, currency)
		}
		currencyChan <- currencies
	}()

	// Запуск горутины для получения доступных криптовалют
	go func() {
		defer wg.Done()
		var cryptoCurrencies []model.Currency

		rows, err := r.db.Query(ctxx, "SELECT id, name, code, available FROM crypto_currencies WHERE available = true")
		if err != nil {
			errorChan <- err
			return
		}
		defer rows.Close()

		for rows.Next() {
			var currency model.Currency
			if err := rows.Scan(&currency.ID, &currency.Name, &currency.Code, &currency.Available); err != nil {
				errorChan <- err
				return
			}
			cryptoCurrencies = append(cryptoCurrencies, currency)
		}
		cryptoCurrencyChan <- cryptoCurrencies
	}()

	go func() {
		wg.Wait()
		close(currencyChan)
		close(cryptoCurrencyChan)
		close(errorChan)
	}()

	var currencies []model.Currency
	var cryptoCurrencies []model.Currency
	var err error

	// Ожидание результатов обеих горутин или ошибки
	for i := 0; i < 2; i++ {
		select {
		case curr := <-currencyChan:
			currencies = curr
		case cryptoCurr := <-cryptoCurrencyChan:
			cryptoCurrencies = cryptoCurr
		case e := <-errorChan:
			err = e
		}
	}
	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("GetAvailableCurrencies")
		return nil, nil, err
	}
	metrics.ObserveDBQueryDuration("GetAvailableCurrencies", time.Since(start).Seconds())

	fmt.Println()
	fmt.Println("------------------- TIME: ", time.Since(start))
	fmt.Println()

	return currencies, cryptoCurrencies, nil
}

func (r *Repository) UpdateRates(ctx context.Context, baseCurrency string, rates map[string]float64, isCrypto bool) error {
	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository:UpdateRates")
	defer span.End()

	start := time.Now()
	var baseCurrencyID int
	var err error
	if isCrypto {
		err = r.db.QueryRow(ctxx, "SELECT id FROM crypto_currencies WHERE code=$1", baseCurrency).Scan(&baseCurrencyID)
	} else {
		err = r.db.QueryRow(ctxx, "SELECT id FROM currencies WHERE code=$1", baseCurrency).Scan(&baseCurrencyID)
	}

	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("GetBaseCurrencyID")
		fmt.Println("Error fetching base currency ID:", err)
		return err
	}
	metrics.ObserveDBQueryDuration("GetBaseCurrencyID", time.Since(start).Seconds())

	fmt.Println("Base Currency ID:", baseCurrencyID)

	for pair, rate := range rates {
		// Extract target currency code from the pair
		targetCurrencyCode := strings.Split(pair, "/")[0]

		var targetCurrencyID int
		if isCrypto {
			err = r.db.QueryRow(ctxx, "SELECT id FROM crypto_currencies WHERE code=$1", targetCurrencyCode).Scan(&targetCurrencyID)
		} else {
			err = r.db.QueryRow(ctxx, "SELECT id FROM currencies WHERE code=$1", targetCurrencyCode).Scan(&targetCurrencyID)
		}

		if err != nil {
			metrics.IncrementDBQueryErrorsTotal("GetTargetCurrencyID")
			fmt.Println("Error fetching target currency ID for code", targetCurrencyCode, ":", err)
			continue // Skip this pair and continue with the next one
		}
		metrics.ObserveDBQueryDuration("GetTargetCurrencyID", time.Since(start).Seconds())
		fmt.Println("Target Currency ID for code", targetCurrencyCode, ":", targetCurrencyID)

		rateInSmallestUnit := int64(rate * 100000000) // Преобразование курса в наименьшие единицы

		if isCrypto {
			_, err = r.db.Exec(ctxx, `
				INSERT INTO crypto_currency_rates (base_currency_id, target_currency_id, rate, last_updated)
				VALUES ($1, $2, $3, CURRENT_TIMESTAMP)
				ON CONFLICT (base_currency_id, target_currency_id)
				DO UPDATE SET rate = EXCLUDED.rate, last_updated = EXCLUDED.last_updated
			`, baseCurrencyID, targetCurrencyID, rateInSmallestUnit)
		} else {
			_, err = r.db.Exec(ctxx, `
				INSERT INTO currency_rates (base_currency_id, target_currency_id, rate, last_updated)
				VALUES ($1, $2, $3, CURRENT_TIMESTAMP)
				ON CONFLICT (base_currency_id, target_currency_id)
				DO UPDATE SET rate = EXCLUDED.rate, last_updated = EXCLUDED.last_updated
			`, baseCurrencyID, targetCurrencyID, rateInSmallestUnit)
		}

		if err != nil {
			metrics.IncrementDBQueryErrorsTotal("InsertUpdateRate")
			fmt.Println("Error inserting/updating rate for target currency ID", targetCurrencyID, ":", err)
			continue // Skip this pair and continue with the next one
		}
		metrics.ObserveDBQueryDuration("InsertUpdateRate", time.Since(start).Seconds())
		fmt.Println("Inserted/Updated rate for target currency ID", targetCurrencyID, "with rate", rateInSmallestUnit)
	}
	return nil
}
