package model

type Currency struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Code      string `json:"code"`
	Available bool   `json:"available"`
}

type CurrencyRate struct {
	ID               int    `json:"id"`
	BaseCurrencyID   int    `json:"base_currency_id"`
	TargetCurrencyID int    `json:"target_currency_id"`
	Rate             int64  `json:"rate"` // Rate в наименьших единицах
	LastUpdated      string `json:"last_updated"`
}

type CryptoCurrencyResponse struct {
	Currencies map[string]string `json:"currencies"`
}
