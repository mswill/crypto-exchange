package usecase

import (
	"context"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/mswill/updater-service/internal/repository"
	"gitlab.com/mswill/updater-service/kafka"
	"gitlab.com/mswill/updater-service/pkg/external"
	"gitlab.com/mswill/updater-service/pkg/metrics"
	"gitlab.com/mswill/updater-service/pkg/tracing"
	"go.opentelemetry.io/otel/trace"
	"time"
)

type UseCase struct {
	repo      *repository.Repository
	apiClient *external.FastForexClient
	producer  *kafka.KafkaProducer
	tr        trace.Tracer
}

func NewUseCase(repo *repository.Repository, apiClient *external.FastForexClient, producer *kafka.KafkaProducer, tr trace.Tracer) *UseCase {
	return &UseCase{
		repo:      repo,
		apiClient: apiClient,
		producer:  producer,
		tr:        tr,
	}
}

// UseCase
func (uc *UseCase) UpdateRates(ctx context.Context) error {
	ctxx, span := tracing.StartSpan(ctx, uc.tr, "UseCase:UpdateRates")
	defer span.End()

	start := time.Now()
	// Получение кодов валют и криптовалют
	currencies, _, err := uc.repo.GetAvailableCurrencies(ctxx)
	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("GetAvailableCurrencies")
		return err
	}
	metrics.ObserveDBQueryDuration("GetAvailableCurrencies", time.Since(start).Seconds())
	// Извлечение кодов обычных валют
	var currencyCodes []string
	for _, currency := range currencies {
		currencyCodes = append(currencyCodes, currency.Code)
	}

	baseCurrency := "USD"

	start = time.Now()
	// Получение курсов обычных валют
	rates, err := uc.apiClient.FetchRates(ctxx, baseCurrency, currencyCodes)
	if err != nil {
		return err
	}
	metrics.ObserveDBQueryDuration("FetchRates", time.Since(start).Seconds())

	start = time.Now()
	// Обновление курсов в базе данных
	if err := uc.repo.UpdateRates(ctxx, baseCurrency, rates, false); err != nil {
		metrics.IncrementDBQueryErrorsTotal("UpdateRates")
		return err
	}
	metrics.ObserveDBQueryDuration("UpdateRates", time.Since(start).Seconds())

	start = time.Now()
	// Отправка сообщения в Kafka после успешного обновления курсов
	message := map[string]interface{}{
		"event":   "rates_updated",
		"time":    time.Now().Format(time.RFC3339),
		"message": "Венчурная валюта",
	}

	if err := uc.producer.SendMessage(ctxx, message); err != nil {
		metrics.IncrementKafkaMessagesErrorsTotal("rates_updated")
		log.Printf("Failed to send message to Kafka: %v", err)
		return err
	}
	metrics.IncrementKafkaMessagesSentTotal("rates_updated")
	return nil
}

func (uc *UseCase) UpdateCryptoRates(ctx context.Context) error {
	ctxx, span := tracing.StartSpan(ctx, uc.tr, "UseCase:Crypto:UpdateCryptoRates")
	defer span.End()

	start := time.Now()
	// Получение кодов валют и криптовалют
	_, cryptoCurrencies, err := uc.repo.GetAvailableCurrencies(ctxx)
	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("GetAvailableCurrencies")
		return err
	}
	metrics.ObserveDBQueryDuration("GetAvailableCurrencies", time.Since(start).Seconds())

	// Извлечение кодов криптовалют
	var cryptoCurrencyCodes []string
	for _, currency := range cryptoCurrencies {
		cryptoCurrencyCodes = append(cryptoCurrencyCodes, currency.Code)
	}

	baseCurrency := "USDT"

	// Формируем пары для получения курсов
	var pairs []string
	for _, code := range cryptoCurrencyCodes {
		pairs = append(pairs, fmt.Sprintf("%s/%s", code, baseCurrency))
	}

	start = time.Now()
	// Получение курсов криптовалют
	cryptoRates, err := uc.apiClient.FetchCryptoRates(ctxx, pairs)
	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("FetchCryptoRates")
		log.Error().Err(err).Msg("Failed to fetch crypto rates")
		return err
	}
	metrics.ObserveDBQueryDuration("FetchCryptoRates", time.Since(start).Seconds())

	if len(cryptoRates) == 0 {
		return fmt.Errorf("no crypto rates found for pairs %s", pairs)
	}

	fmt.Println()
	fmt.Println()
	fmt.Println("BASE ", baseCurrency)
	fmt.Println("cryptoRates ", cryptoRates)
	fmt.Println()
	fmt.Println()

	start = time.Now()
	// Обновление курсов криптовалют в базе данных
	if err := uc.repo.UpdateRates(ctxx, baseCurrency, cryptoRates, true); err != nil {
		metrics.IncrementDBQueryErrorsTotal("UpdateCryptoRates")
		log.Error().Err(err).Msg("Failed to update crypto rates in database")
		return err
	}
	metrics.ObserveDBQueryDuration("UpdateCryptoRates", time.Since(start).Seconds())

	log.Info().Msg("Successfully updated crypto rates")

	// Отправка сообщения в Kafka после успешного обновления курсов криптовалют
	message := map[string]interface{}{
		"event":   "crypto_rates_updated",
		"time":    time.Now().Format(time.RFC3339),
		"message": "Крипто валюта",
	}

	if err := uc.producer.SendMessage(ctxx, message); err != nil {
		metrics.IncrementKafkaMessagesErrorsTotal("rates_updated")
		log.Printf("Failed to send message to Kafka: %v", err)
		return err
	}
	metrics.IncrementKafkaMessagesSentTotal("rates_updated")
	return nil
}
