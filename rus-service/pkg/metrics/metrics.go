package metrics

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	// HTTP request metrics
	httpRequestsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "_rus_http_requests_total",
			Help: "Total number of HTTP requests",
		},
		[]string{"method", "endpoint", "status"},
	)

	httpRequestDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "_rus_http_request_duration_seconds",
			Help:    "Duration of HTTP requests in seconds",
			Buckets: prometheus.DefBuckets,
		},
		[]string{"method", "endpoint"},
	)

	// Database metrics
	dbQueryDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "_rus_db_query_duration_seconds",
			Help:    "Duration of database queries in seconds",
			Buckets: prometheus.DefBuckets,
		},
		[]string{"query"},
	)

	dbQueryErrorsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "_rus_db_query_errors_total",
			Help: "Total number of database query errors",
		},
		[]string{"query"},
	)

	// Kafka metrics
	kafkaMessagesSentTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "_rus_kafka_messages_sent_total",
			Help: "Total number of Kafka messages sent",
		},
		[]string{"topic"},
	)

	kafkaMessagesErrorsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "_rus_kafka_messages_errors_total",
			Help: "Total number of Kafka message errors",
		},
		[]string{"topic"},
	)
)

func init() {
	prometheus.MustRegister(httpRequestsTotal)
	prometheus.MustRegister(httpRequestDuration)
	prometheus.MustRegister(dbQueryDuration)
	prometheus.MustRegister(dbQueryErrorsTotal)
	prometheus.MustRegister(kafkaMessagesSentTotal)
	prometheus.MustRegister(kafkaMessagesErrorsTotal)
}

func MetricsHandler() http.Handler {
	return promhttp.Handler()
}

func ObserveDBQueryDuration(query string, duration float64) {
	dbQueryDuration.WithLabelValues(query).Observe(duration)
}

func IncrementDBQueryErrorsTotal(query string) {
	dbQueryErrorsTotal.WithLabelValues(query).Inc()
}

func IncrementKafkaMessagesSentTotal(topic string) {
	kafkaMessagesSentTotal.WithLabelValues(topic).Inc()
}

func IncrementKafkaMessagesErrorsTotal(topic string) {
	kafkaMessagesErrorsTotal.WithLabelValues(topic).Inc()
}
