package consul

import (
	"fmt"
	"github.com/hashicorp/consul/api"
	"github.com/rs/zerolog/log"
)

type ConsulClient struct {
	client *api.Client
}

func NewConsulClient(address string) (*ConsulClient, error) {
	config := api.DefaultConfig()
	config.Address = address

	client, err := api.NewClient(config)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to create Consul client")
		return nil, fmt.Errorf("failed to create Consul client: %v", err)
	}

	log.Info().Msgf("Consul client created with address: %s", address)
	return &ConsulClient{client: client}, nil
}

func (cc *ConsulClient) RegisterService(serviceName string, servicePort int) error {
	registration := &api.AgentServiceRegistration{
		ID:      serviceName,
		Name:    serviceName,
		Address: "rus",
		Port:    servicePort,
		Check: &api.AgentServiceCheck{
			HTTP:     fmt.Sprintf("http://rus:%d/health", servicePort),
			Interval: "5s",
			Timeout:  "5s",
		},
	}

	log.Info().Msgf("Registering service %s on port %d", serviceName, servicePort)
	err := cc.client.Agent().ServiceRegister(registration)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to register service with Consul")
		return fmt.Errorf("failed to register service with Consul: %v", err)
	}

	log.Info().Msg("Service registered with Consul successfully")
	return nil
}
