package external

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/mswill/updater-service/pkg/tracing"
	"go.opentelemetry.io/otel/trace"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type FastForexClient struct {
	apiKey string
	url    string
	client *http.Client
	tr     trace.Tracer
}

type RateResponse struct {
	Base    string             `json:"base"`
	Results map[string]float64 `json:"results"` // Используем "results" вместо "rates"
}

func NewFastForexClient(apiKey, url string, tr trace.Tracer) *FastForexClient {
	return &FastForexClient{
		apiKey: apiKey,
		url:    url,
		client: &http.Client{Timeout: 10 * time.Second},
		tr:     tr,
	}
}

func (c *FastForexClient) FetchRates(ctx context.Context, baseCurrency string, currencies []string) (map[string]float64, error) {
	_, span := tracing.StartSpan(ctx, c.tr, "FastForexClient:FetchRates")
	defer span.End()

	url := fmt.Sprintf("%s/fetch-multi?api_key=%s&from=%s&to=%s", c.url, c.apiKey, baseCurrency, strings.Join(currencies, ","))
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var rateResponse struct {
		Results map[string]float64 `json:"results"`
	}
	if err := json.Unmarshal(body, &rateResponse); err != nil {
		return nil, err
	}

	if len(rateResponse.Results) == 0 {
		return nil, fmt.Errorf("no rates found for base currency %s", baseCurrency)
	}

	fmt.Println("----- Получили венчурную валюту: ", rateResponse.Results)
	return rateResponse.Results, nil
}

func (c *FastForexClient) FetchCryptoRates(ctx context.Context, pairs []string) (map[string]float64, error) {
	_, span := tracing.StartSpan(ctx, c.tr, "FastForexClient:FetchCryptoRates")
	defer span.End()

	const maxPairsPerRequest = 10
	allRates := make(map[string]float64)

	for i := 0; i < len(pairs); i += maxPairsPerRequest {
		end := i + maxPairsPerRequest
		if end > len(pairs) {
			end = len(pairs)
		}
		subset := pairs[i:end]
		url := fmt.Sprintf("%s/crypto/fetch-prices?api_key=%s&pairs=%s", c.url, c.apiKey, strings.Join(subset, ","))
		fmt.Println("URL for fetching crypto rates: ", url) // Логируем URL

		resp, err := http.Get(url)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		fmt.Println("Raw response body for crypto rates: ", string(body)) // Логируем ответ

		var priceResponse struct {
			Prices map[string]float64 `json:"prices"`
		}
		if err := json.Unmarshal(body, &priceResponse); err != nil {
			return nil, err
		}

		if len(priceResponse.Prices) == 0 {
			return nil, fmt.Errorf("no crypto prices found for pairs %s", subset)
		}

		for k, v := range priceResponse.Prices {
			allRates[k] = v
		}
	}

	fmt.Println("----- Получили криптовалюту: ", allRates) // Логируем курсы
	return allRates, nil
}
