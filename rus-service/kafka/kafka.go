package kafka

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/mswill/updater-service/pkg/metrics"
	"gitlab.com/mswill/updater-service/pkg/tracing"
	"go.opentelemetry.io/otel/trace"
	"os"
	"time"

	"github.com/segmentio/kafka-go"
)

type KafkaProducer struct {
	writer *kafka.Writer
	tr     trace.Tracer
}

func NewKafkaProducer(brokers []string, topic string, tr trace.Tracer) *KafkaProducer {
	fmt.Println()
	fmt.Println("brokers ", brokers)
	fmt.Println()
	bkrs := []string{os.Getenv("KAFKA_ADDR_0"), os.Getenv("KAFKA_ADDR_1")}
	writer := &kafka.Writer{
		Addr:         kafka.TCP(bkrs...),
		Topic:        topic,
		Balancer:     &kafka.LeastBytes{},
		RequiredAcks: kafka.RequireOne,
	}
	return &KafkaProducer{writer: writer, tr: tr}
}

func (kp *KafkaProducer) SendMessage(ctx context.Context, message interface{}) error {
	ctxx, span := tracing.StartSpan(ctx, kp.tr, "KafkaProducer:SendMessage")
	defer span.End()

	msgBytes, err := json.Marshal(message)
	if err != nil {
		metrics.IncrementKafkaMessagesErrorsTotal(kp.writer.Topic)
		return err
	}

	err = kp.writer.WriteMessages(ctxx, kafka.Message{
		Key:   []byte(time.Now().String()),
		Value: msgBytes,
	})

	if err != nil {
		metrics.IncrementKafkaMessagesErrorsTotal(kp.writer.Topic)
		return err
	}

	metrics.IncrementKafkaMessagesSentTotal(kp.writer.Topic)
	return nil
}

func (kp *KafkaProducer) Close() error {
	return kp.writer.Close()
}
