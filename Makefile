updater = updater-service

#---
make = docker-compose -f ./docker-compose.yaml up --build
makeDev = docker-compose -f ./docker-compose-dev.yaml up --build

up:
	$(info  "  ---- Starting PRODUCTION docker images ---- ")
	$(make) --build

dev:
	$(info  " ---- Starting DEVELOPMENT docker images ---- ")
	$(makeDev)


ddev:
	$(info  " ---- Stopping  docker images ---- ")
	docker-compose -f ./docker-compose-dev.yaml down

down:
	$(info  " ---- Stopping  docker images ---- ")
	docker-compose -f ./docker-compose.yaml down

cp:
	$(info  " ---- Clean docker containers prune ---- ")
	docker container prune
