package server

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"

	"gitlab.com/mswill/cms-service/internal/controllers"
)

// StartServer инициализирует и запускает Fiber сервер
func StartServer(app *fiber.App, ctrl *controllers.Controller) {

	app.Use(logger.New())

	// Define routes
	app.Get("/currencies", ctrl.GetCurrencies)

	app.Post("/currencies", ctrl.AddCurrency)
	app.Post("/crypto-currencies", ctrl.AddCryptoCurrency)

	app.Delete("/currencies/:code", ctrl.DeleteCurrency)
	app.Delete("/crypto-currencies/:code", ctrl.DeleteCryptoCurrency)

	app.Patch("/currencies/:code/availability", ctrl.UpdateAvailability)
	app.Patch("/crypto-currencies/:code/availability", ctrl.UpdateCryptoAvailability)

	// consule check hanlder
	app.Get("/health", ctrl.Check)
	serverAddress := viper.GetString("server.address")
	serverPort := viper.GetString("server.port")
	// Start server
	go func() {
		if err := app.Listen(serverAddress + ":" + serverPort); err != nil {
			log.Fatal().Err(err).Msg("Failed to start server")
		}
	}()

	log.Info().Msg("Currency Management Service started")

	// Graceful shutdown
	shutdownChan := make(chan os.Signal, 1)
	signal.Notify(shutdownChan, syscall.SIGTERM, syscall.SIGINT)
	<-shutdownChan

	log.Info().Msg("Shutting down gracefully...")

	if err := app.Shutdown(); err != nil {
		log.Fatal().Err(err).Msg("Failed to gracefully shut down server")
	}

	log.Info().Msg("Server stopped")
}
