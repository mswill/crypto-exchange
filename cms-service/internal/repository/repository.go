package repository

import (
	"context"
	"fmt"
	s "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/mswill/cms-service/pkg/tracing"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/mswill/cms-service/internal/model"
)

var (
	currencies       = "currencies"
	currencyRates    = "currency_rates"
	cryptoCurrencies = "crypto_currencies"
)

type Repository struct {
	db *pgxpool.Pool
	s  s.StatementBuilderType
	tr trace.Tracer
}

func NewRepository(db *pgxpool.Pool, tr trace.Tracer) *Repository {
	return &Repository{
		db: db,
		s:  s.StatementBuilder.PlaceholderFormat(s.Dollar),
		tr: tr,
	}
}

func (r *Repository) GetCurrencies(ctx context.Context) ([]model.Currency, error) {
	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository.GetCurrencies")
	defer span.End()

	rows, err := r.db.Query(ctxx, "SELECT id, name, code, available FROM currencies UNION SELECT id, name, code, available FROM crypto_currencies")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var currencies []model.Currency
	for rows.Next() {
		var currency model.Currency
		if err := rows.Scan(&currency.ID, &currency.Name, &currency.Code, &currency.Available); err != nil {
			return nil, err
		}
		currencies = append(currencies, currency)
	}

	return currencies, nil
}

func (r *Repository) AddCurrency(ctx context.Context, currency *model.Currency) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository.AddCurrency")
	defer span.End()

	sql, args, err := r.s.
		Insert(currencies).
		Columns("id", "name", "code", "available").
		Values(currency.ID, currency.Name, currency.Code, currency.Available).
		ToSql()
	if err != nil {
		return "", err
	}

	exec, err := r.db.Exec(ctxx, sql, args...)
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	if exec.RowsAffected() != 0 {
		return "ok", err
	}

	return "", err
}
func (r *Repository) AddCryptoCurrency(ctx context.Context, currency *model.Currency) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository.AddCryptoCurrency")
	defer span.End()

	sql, args, err := r.s.
		Insert(cryptoCurrencies).
		Columns("id", "name", "code", "available").
		Values(currency.ID, currency.Name, currency.Code, currency.Available).
		ToSql()
	if err != nil {
		return "", err
	}

	exec, err := r.db.Exec(ctxx, sql, args...)
	if err != nil {
		return "", err
	}

	if exec.RowsAffected() != 0 {
		return "ok", err
	}

	return "", err
}

func (r *Repository) DeleteCurrency(ctx context.Context, code string) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository.DeleteCurrency")
	defer span.End()

	sql, args, err := r.s.
		Delete(currencies).
		Where(s.Eq{"code": code}).
		ToSql()

	if err != nil {
		return "", err
	}

	exec, err := r.db.Exec(ctxx, sql, args...)
	if err != nil {
		return "", err
	}

	if exec.RowsAffected() != 0 {
		return "ok", err
	}

	return "", err
}
func (r *Repository) DeleteCryptoCurrency(ctx context.Context, code string) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository.DeleteCryptoCurrency")
	defer span.End()

	sql, args, err := r.s.
		Delete(cryptoCurrencies).
		Where(s.Eq{"code": code}).
		ToSql()

	if err != nil {
		return "", err
	}

	exec, err := r.db.Exec(ctxx, sql, args...)
	if err != nil {
		return "", err
	}

	if exec.RowsAffected() != 0 {
		return "ok", err
	}

	return "", err
}

func (r *Repository) UpdateAvailability(ctx context.Context, code string, up *model.UpdateCurrency) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository.UpdateAvailability")
	defer span.End()

	sql := `
		UPDATE currencies
		SET
			available = COALESCE(NULLIF($1::boolean, null), available),
			code = COALESCE(NULLIF($2, ''), code),
			name = COALESCE(NULLIF($3, ''), name)
		WHERE code = $4
	`
	// Используем указатель на bool для различения неопределенного значения
	var available bool
	if up.Available {
		available = up.Available
	}

	args := []interface{}{available, up.Code, up.Name, code}

	exec, err := r.db.Exec(ctxx, sql, args...)
	if err != nil {
		return "", err
	}

	rowsAffected := exec.RowsAffected()

	if rowsAffected != 0 {
		return "ok", nil
	}

	return "", err
}
func (r *Repository) UpdateCryptoAvailability(ctx context.Context, code string, up *model.UpdateCurrency) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, r.tr, "Repository.UpdateCryptoAvailability")
	defer span.End()

	sql := `
		UPDATE crypto_currencies
		SET
			available = COALESCE(NULLIF($1::boolean, null), available),
			code = COALESCE(NULLIF($2, ''), code),
			name = COALESCE(NULLIF($3, ''), name)
		WHERE code = $4
	`
	// Используем указатель на bool для различения неопределенного значения
	var available bool
	if up.Available {
		available = up.Available
	}

	args := []interface{}{available, up.Code, up.Name, code}

	exec, err := r.db.Exec(ctxx, sql, args...)
	if err != nil {
		return "", err
	}

	rowsAffected := exec.RowsAffected()

	if rowsAffected != 0 {
		return "ok", nil
	}

	return "", err
}
