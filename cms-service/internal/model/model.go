package model

// swagger:model principal
type Currency struct {
	ID        int32  `json:"id,omitempty"`
	Name      string `json:"name"`
	Code      string `json:"code"`
	Available bool   `json:"available"`
}

// swagger:model principal
type UpdateCurrency struct {
	Name      string `json:"name,omitempty"`
	Code      string `json:"code,omitempty"`
	Available bool   `json:"available"`
}
