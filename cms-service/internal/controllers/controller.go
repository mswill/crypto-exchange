package controllers

import (
	"errors"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mswill/cms-service/internal/model"
	"gitlab.com/mswill/cms-service/internal/usecase"
	"gitlab.com/mswill/cms-service/pkg/metrics"
	"gitlab.com/mswill/cms-service/pkg/tracing"
	"go.opentelemetry.io/otel/trace"
	"math/rand"
	"strings"
	"time"
)

type Controller struct {
	usecase *usecase.UseCase
	tr      trace.Tracer
}

func NewController(uc *usecase.UseCase, tr trace.Tracer) *Controller {
	return &Controller{
		usecase: uc,
		tr:      tr,
	}
}

// GetCurrencies Get all currencies
// @Summary Get all currencies
// @Description Get all available currencies
// @ID  get_currencies_get
// @Tags all currencies
// @Accept  json
// @Produce  json
// @Success 200 {array} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Router /currencies [get]
func (c *Controller) GetCurrencies(ctx *fiber.Ctx) error {
	start := time.Now()
	ctxx, span := tracing.StartSpan(ctx.Context(), c.tr, "/currencies [get]")
	defer span.End()
	currencies, err := c.usecase.GetCurrencies(ctxx)
	duration := time.Since(start).Seconds()
	if err != nil {
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "500")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.
			Status(fiber.StatusInternalServerError).
			JSON(fiber.Map{"error": err.Error()})
	}

	metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "200")
	metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
	return ctx.JSON(currencies)
}

// AddCurrency Add a new base currency
// @Summary Add base a new currency
// @Description Add base a new currency to the list
// @ID  get_currencies_post
// @Tags currencies
// @Accept  json
// @Produce  json
// @Param currency body model.Currency true "Currency"
// @Success 201 {object} model.Currency
// @Failure 400 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Router /currencies [post]
func (c *Controller) AddCurrency(ctx *fiber.Ctx) error {
	start := time.Now()
	ctxx, span := tracing.StartSpan(ctx.Context(), c.tr, "/currencies [post]")
	defer span.End()

	var currency model.Currency
	if err := ctx.BodyParser(&currency); err != nil {
		duration := time.Since(start).Seconds()
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "400")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.Status(fiber.StatusBadRequest).JSON(map[string]interface{}{"error": err.Error()})
	}

	if currency.Code == "" {
		duration := time.Since(start).Seconds()
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "400")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.JSON(fiber.Map{"error": errors.New("CODE is required field for currency").Error()})
	}
	currency.ID = setId()
	currency.Code = toUpper(currency.Code)

	addCurrency, err := c.usecase.AddCurrency(ctxx, &currency)
	duration := time.Since(start).Seconds()
	if err != nil {
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "500")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.
			Status(fiber.StatusInternalServerError).
			JSON(fiber.Map{"error": err.Error()})
	}

	metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "201")
	metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
	return ctx.JSON(fiber.Map{"status": addCurrency})
}

// AddCryptoCurrency Add crypto a new currency
// @Summary Add crypto a new currency
// @Description Add crypto a new currency to the list
// @ID  get_crypto_currencies_post
// @Tags crypto_currencies
// @Accept  json
// @Produce  json
// @Param currency body model.Currency true "Currency"
// @Success 201 {object} model.Currency
// @Failure 400 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Router /crypto-currencies [post]
func (c *Controller) AddCryptoCurrency(ctx *fiber.Ctx) error {
	start := time.Now()
	ctxx, span := tracing.StartSpan(ctx.Context(), c.tr, "/crypto-currencies [post]")
	defer span.End()

	var currency model.Currency
	if err := ctx.BodyParser(&currency); err != nil {
		duration := time.Since(start).Seconds()
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "400")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.Status(fiber.StatusBadRequest).JSON(map[string]interface{}{"error": err.Error()})
	}

	if currency.Code == "" {
		duration := time.Since(start).Seconds()
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "400")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.JSON(fiber.Map{"error": errors.New("CODE is required field for crypto currency").Error()})
	}
	currency.ID = setId()
	currency.Code = toUpper(currency.Code)
	addCurrency, err := c.usecase.AddCryptoCurrency(ctxx, &currency)
	duration := time.Since(start).Seconds()
	if err != nil {
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "500")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.
			Status(fiber.StatusInternalServerError).
			JSON(fiber.Map{"error": err.Error()})
	}

	metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "201")
	metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
	return ctx.JSON(fiber.Map{"status": addCurrency})
}

// DeleteCurrency Delete a currency
// @Summary Delete a currency
// @Description Delete a currency by code
// @ID currencies_del
// @Tags currencies
// @Accept  json
// @Produce  json
// @Param code path string true "Currency Code"
// @Success 200 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Router /currencies/{code} [delete]
func (c *Controller) DeleteCurrency(ctx *fiber.Ctx) error {
	start := time.Now()
	ctxx, span := tracing.StartSpan(ctx.Context(), c.tr, "/currencies/{code} [delete]")
	defer span.End()
	code := ctx.Params("code")

	code = toUpper(code)
	currency, err := c.usecase.DeleteCurrency(ctxx, code)
	duration := time.Since(start).Seconds()
	if err != nil {
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "500")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.
			Status(fiber.StatusInternalServerError).
			JSON(fiber.Map{"error": err.Error()})
	}

	metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "200")
	metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
	return ctx.JSON(fiber.Map{"status": currency})
}

// DeleteCryptoCurrency Delete a crypto currency
// @Summary Delete a crypto_currency
// @Description Delete a crypto_currency by code
// @ID crypto_currencies_del
// @Tags crypto_currencies
// @Accept  json
// @Produce  json
// @Param code path string true "Currency Code"
// @Success 200 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Router /crypto-currencies/{code} [delete]
func (c *Controller) DeleteCryptoCurrency(ctx *fiber.Ctx) error {
	start := time.Now()
	ctxx, span := tracing.StartSpan(ctx.Context(), c.tr, "/crypto-currencies/{code} [delete]")
	defer span.End()

	code := ctx.Params("code")

	code = toUpper(code)
	currency, err := c.usecase.DeleteCryptoCurrency(ctxx, code)
	duration := time.Since(start).Seconds()
	if err != nil {
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "500")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.
			Status(fiber.StatusInternalServerError).
			JSON(fiber.Map{"error": err.Error()})
	}

	metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "200")
	metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
	return ctx.JSON(fiber.Map{"status": currency})
}

// UpdateAvailability Update currency availability
// @Summary Update currency availability
// @Description Update the availability status of a currency by code
// @ID currencies_update
// @Tags currencies
// @Accept  json
// @Produce  json
// @Param code path string true "Currency Code"
// @Param availability body model.UpdateCurrency   true "Availability"
// @Success 204
// @Failure 400 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Router /currencies/{code}/availability [patch]
func (c *Controller) UpdateAvailability(ctx *fiber.Ctx) error {
	start := time.Now()
	ctxx, span := tracing.StartSpan(ctx.Context(), c.tr, "/currencies/{code}/availability [patch]")
	defer span.End()

	code := ctx.Params("code")
	var up model.UpdateCurrency

	if code == "" {
		duration := time.Since(start).Seconds()
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "400")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.JSON(fiber.Map{"error": errors.New("CODE is required field").Error()})
	}
	code = toUpper(code)
	up.Code = toUpper(up.Code)
	if err := ctx.BodyParser(&up); err != nil {
		duration := time.Since(start).Seconds()
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "400")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.
			Status(fiber.StatusBadRequest).
			JSON(fiber.Map{"error": err.Error()})
	}

	availability, err := c.usecase.UpdateAvailability(ctxx, code, &up)
	duration := time.Since(start).Seconds()
	if err != nil {
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "500")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.
			Status(fiber.StatusInternalServerError).
			JSON(fiber.Map{"error": err.Error()})
	}

	metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "204")
	metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
	return ctx.JSON(fiber.Map{"status": availability})
}

// UpdateCryptoAvailability Update crypto currency availability
// @Summary Update crypto currency availability
// @Description Update the availability status of a crypto currency by code
// @ID crypto_currencies_update
// @Tags crypto_currencies
// @Accept  json
// @Produce  json
// @Param code path string true "Crypto Currency Code"
// @Param availability body model.UpdateCurrency   true "Availability"
// @Success 204
// @Failure 400 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Router /crypto-currencies/{code}/availability [patch]
func (c *Controller) UpdateCryptoAvailability(ctx *fiber.Ctx) error {
	start := time.Now()
	ctxx, span := tracing.StartSpan(ctx.Context(), c.tr, "/currencies/{code}/availability [patch]")
	defer span.End()

	code := ctx.Params("code")
	var up model.UpdateCurrency

	if code == "" {
		duration := time.Since(start).Seconds()
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "400")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.JSON(fiber.Map{"error": errors.New("CODE is required field").Error()})
	}
	code = toUpper(code)
	up.Code = toUpper(up.Code)
	if err := ctx.BodyParser(&up); err != nil {
		duration := time.Since(start).Seconds()
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "400")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.
			Status(fiber.StatusBadRequest).
			JSON(fiber.Map{"error": err.Error()})
	}

	availability, err := c.usecase.UpdateCryptoAvailability(ctxx, code, &up)
	duration := time.Since(start).Seconds()
	if err != nil {
		metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "500")
		metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
		return ctx.
			Status(fiber.StatusInternalServerError).
			JSON(fiber.Map{"error": err.Error()})
	}

	metrics.IncrementHTTPRequestsTotal(ctx.Method(), ctx.Path(), "204")
	metrics.ObserveHTTPRequestDuration(ctx.Method(), ctx.Path(), duration)
	return ctx.JSON(fiber.Map{"status": availability})
}

func (c *Controller) Check(ctx *fiber.Ctx) error {
	return ctx.SendStatus(fiber.StatusOK)
}

func setId() int32 {
	rand.NewSource(time.Now().UnixNano())
	id := rand.Int31()
	return id
}

func toUpper(code string) string {
	return strings.ToUpper(code)
}
