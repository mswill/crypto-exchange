package usecase

import (
	"context"
	"gitlab.com/mswill/cms-service/internal/model"
	"gitlab.com/mswill/cms-service/internal/repository"
	"gitlab.com/mswill/cms-service/pkg/metrics"
	"gitlab.com/mswill/cms-service/pkg/tracing"
	"go.opentelemetry.io/otel/trace"
	"time"
)

type Repository interface {
	GetCurrencies(ctx context.Context) ([]model.Currency, error)
	AddCurrency(ctx context.Context, currency *model.Currency) (string, error)
	AddCryptoCurrency(ctx context.Context, currency *model.Currency) (string, error)
	DeleteCurrency(ctx context.Context, code string) (string, error)
	DeleteCryptoCurrency(ctx context.Context, code string) (string, error)
	UpdateAvailability(ctx context.Context, code string, up *model.UpdateCurrency) (string, error)
	UpdateCryptoAvailability(ctx context.Context, code string, up *model.UpdateCurrency) (string, error)
}
type UseCase struct {
	repo Repository
	tr   trace.Tracer
}

func NewUseCase(repo *repository.Repository, tr trace.Tracer) *UseCase {
	return &UseCase{repo: repo, tr: tr}
}

func (uc *UseCase) GetCurrencies(ctx context.Context) ([]model.Currency, error) {
	ctxx, span := tracing.StartSpan(ctx, uc.tr, "UseCase.GetCurrencies")
	defer span.End()

	start := time.Now()
	currencies, err := uc.repo.GetCurrencies(ctxx)
	duration := time.Since(start).Seconds()
	metrics.ObserveDBQueryDuration("GetCurrencies", duration)

	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("GetCurrencies")
	}

	return currencies, err
}

func (uc *UseCase) AddCurrency(ctx context.Context, currency *model.Currency) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, uc.tr, "UseCase.AddCurrency")
	defer span.End()

	start := time.Now()
	result, err := uc.repo.AddCurrency(ctxx, currency)
	duration := time.Since(start).Seconds()
	metrics.ObserveDBQueryDuration("AddCurrency", duration)

	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("AddCurrency")
	}

	return result, err
}

func (uc *UseCase) AddCryptoCurrency(ctx context.Context, currency *model.Currency) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, uc.tr, "UseCase.AddCryptoCurrency")
	defer span.End()

	start := time.Now()
	result, err := uc.repo.AddCryptoCurrency(ctxx, currency)
	duration := time.Since(start).Seconds()
	metrics.ObserveDBQueryDuration("AddCryptoCurrency", duration)

	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("AddCryptoCurrency")
	}

	return result, err
}

func (uc *UseCase) DeleteCurrency(ctx context.Context, code string) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, uc.tr, "UseCase.DeleteCurrency")
	defer span.End()

	start := time.Now()
	result, err := uc.repo.DeleteCurrency(ctxx, code)
	duration := time.Since(start).Seconds()
	metrics.ObserveDBQueryDuration("DeleteCurrency", duration)

	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("DeleteCurrency")
	}

	return result, err
}

func (uc *UseCase) DeleteCryptoCurrency(ctx context.Context, code string) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, uc.tr, "UseCase.DeleteCryptoCurrency")
	defer span.End()

	start := time.Now()
	result, err := uc.repo.DeleteCryptoCurrency(ctxx, code)
	duration := time.Since(start).Seconds()
	metrics.ObserveDBQueryDuration("DeleteCryptoCurrency", duration)

	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("DeleteCryptoCurrency")
	}

	return result, err
}

func (uc *UseCase) UpdateAvailability(ctx context.Context, code string, up *model.UpdateCurrency) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, uc.tr, "UseCase.UpdateAvailability")
	defer span.End()

	start := time.Now()
	result, err := uc.repo.UpdateAvailability(ctxx, code, up)
	duration := time.Since(start).Seconds()
	metrics.ObserveDBQueryDuration("UpdateAvailability", duration)

	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("UpdateAvailability")
	}

	return result, err
}

func (uc *UseCase) UpdateCryptoAvailability(ctx context.Context, code string, up *model.UpdateCurrency) (string, error) {
	ctxx, span := tracing.StartSpan(ctx, uc.tr, "UseCase.UpdateCryptoAvailability")
	defer span.End()

	start := time.Now()
	result, err := uc.repo.UpdateCryptoAvailability(ctxx, code, up)
	duration := time.Since(start).Seconds()
	metrics.ObserveDBQueryDuration("UpdateCryptoAvailability", duration)

	if err != nil {
		metrics.IncrementDBQueryErrorsTotal("UpdateCryptoAvailability")
	}

	return result, err
}
