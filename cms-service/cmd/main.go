package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/swagger"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gitlab.com/mswill/cms-service/pkg/consul"
	"gitlab.com/mswill/cms-service/pkg/metrics"
	"gitlab.com/mswill/cms-service/pkg/tracing"
	"net/http"

	"os"
	"os/signal"
	"syscall"

	_ "gitlab.com/mswill/cms-service/docs"
	"gitlab.com/mswill/cms-service/internal/controllers"
	"gitlab.com/mswill/cms-service/internal/repository"
	"gitlab.com/mswill/cms-service/internal/server"
	"gitlab.com/mswill/cms-service/internal/usecase"
	"gitlab.com/mswill/cms-service/pkg/db"
)

func initConfig() {
	viper.AddConfigPath(".")
	viper.AddConfigPath("./config")
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal().Err(err).Msg("Error reading config file")
	}
}

const serviceName = "Currency Management Service"
const endpoint = "http://jaeger:14268/api/traces"

// @title Currency Management Service API
// @version 1.0
// @description Управляет доступностью валют.
// @description Добавляет новые валюты.
// @description Удаляет существующие валюты.
// @description Интерфейс для управления валютами (например, REST API).
// @host localhost:11000
// @BasePath /
func main() {
	initConfig()

	app := fiber.New()

	// Enable CORS for all routes
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*", // Allows all origins
		AllowMethods: "GET,POST,HEAD,PUT,DELETE,PATCH,OPTIONS",
	}))

	// Initialize Consul client
	consulClient, err := consul.NewConsulClient("consul:8500")
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to create Consul client")
	}

	// Register service with Consul
	err = consulClient.RegisterService(serviceName, 11000)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to register service with Consul")
	}

	// Initialize tracing
	tracer, traceFunc := tracing.InitTracer(serviceName, endpoint)
	defer traceFunc()

	// Initialize logger
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	// Initialize database connection
	dbConn, err := db.Connect()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to connect to database")
	}
	defer db.Close(dbConn)

	// Initialize repository and usecase
	repo := repository.NewRepository(dbConn, tracer)
	usecase := usecase.NewUseCase(repo, tracer)
	ctrl := controllers.NewController(usecase, tracer)

	// Swagger setup
	app.Get("/swagger/*", swagger.HandlerDefault)

	// Start server
	go server.StartServer(app, ctrl)

	// Metrics endpoint
	go func() {
		metricsServer := &http.Server{
			Addr:    ":2113",
			Handler: metrics.MetricsHandler(),
		}
		log.Info().Msg("Starting metrics server at :2113")
		if err := metricsServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal().Err(err).Msg("Failed to start metrics server")
		}
	}()

	// Graceful shutdown
	shutdownChan := make(chan os.Signal, 1)
	signal.Notify(shutdownChan, syscall.SIGTERM, syscall.SIGINT)

	// Listen for interrupt
	<-shutdownChan

	// Graceful shutdown
	log.Info().Msg("Shutting down gracefully...")

	db.Close(dbConn)

	log.Info().Msg("Postgres database closed successfully")
}
