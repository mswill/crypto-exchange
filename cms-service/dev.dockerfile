# Use Go 1.20 Alpine image as the base image
FROM golang:1.21-alpine

RUN echo "----------------- CMS SERVICE -----------------"

# Create the app directory and set it as the working directory
RUN mkdir /app
WORKDIR /app

# Install necessary packages and Air
RUN apk add --update make git vim curl gcc libc-dev && \
    curl -sSfL https://raw.githubusercontent.com/cosmtrek/air/master/install.sh | sh -s -- -b $(go env GOPATH)/bin && \
    echo "\n--------------- AIR ---------------"

# Copy go mod and sum files and download dependencies
COPY go.mod .
COPY go.sum .
RUN go mod download

# Copy the rest of the application code
COPY . .

# Expose the application port
EXPOSE 11000

# Start the application using Air
CMD ["air"]
