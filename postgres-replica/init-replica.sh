#!/bin/bash
set -e

echo "Starting replica setup..."

# Переключение на пользователя postgres
gosu postgres bash <<EOF

# Stop PostgreSQL if running
pg_ctl stop -D /var/lib/postgresql/data || true

# Clean existing data directory
echo "Cleaning existing data directory..."
rm -rf /var/lib/postgresql/data/*

# Remove temporary data directory if exists
if [ -d /var/lib/postgresql/temp_data ]; then
  echo "Removing temporary data directory..."
  rm -rf /var/lib/postgresql/temp_data
fi

# Verify temp_data directory removal
if [ -d /var/lib/postgresql/temp_data ]; then
  echo "Error: temp_data directory still exists!"
  ls -la /var/lib/postgresql/temp_data
  exit 1
fi

# Create temporary data directory
echo "Creating temporary data directory..."
mkdir -p /var/lib/postgresql/temp_data

# Copy configuration files
echo "Copying configuration files..."
cp /etc/postgresql/postgresql.conf /var/lib/postgresql/temp_data/
cp /etc/postgresql/pg_hba.conf /var/lib/postgresql/temp_data/

# Ensure proper ownership and permissions for temporary directory
chown -R postgres:postgres /var/lib/postgresql/temp_data
chmod 700 /var/lib/postgresql/temp_data

# Wait for master to be ready
until pg_isready -h postgres-master -U replication; do
  echo "Waiting for master to be ready..."
  sleep 2
done

# Remove temporary data directory again just before starting base backup
if [ -d /var/lib/postgresql/temp_data ]; then
  echo "Removing temporary data directory before starting base backup..."
  rm -rf /var/lib/postgresql/temp_data
fi

# Verify temp_data directory removal again
if [ -d /var/lib/postgresql/temp_data ]; then
  echo "Error: temp_data directory still exists before starting base backup!"
  ls -la /var/lib/postgresql/temp_data
  exit 1
fi

# Create temporary data directory again
echo "Creating temporary data directory before starting base backup..."
mkdir -p /var/lib/postgresql/temp_data
chown -R postgres:postgres /var/lib/postgresql/temp_data
chmod 700 /var/lib/postgresql/temp_data

# Start base backup from master
echo "Starting base backup from master..."
until pg_basebackup -h postgres-master -D /var/lib/postgresql/temp_data -U replication -v -P --wal-method=stream; do
  echo "Waiting for master to be ready..."
  sleep 2
done

# Move data to the main data directory
echo "Moving data to the main data directory..."
cp -r /var/lib/postgresql/temp_data/* /var/lib/postgresql/data/
rm -rf /var/lib/postgresql/temp_data

# Set ownership and permissions
chown -R postgres:postgres /var/lib/postgresql/data
chmod 700 /var/lib/postgresql/data

# Create standby.signal for standby mode
touch /var/lib/postgresql/data/standby.signal

# Append replication settings to postgresql.conf
cat >> /var/lib/postgresql/data/postgresql.conf <<-EOSQL
primary_conninfo = 'host=postgres-master port=5432 user=replication password=replicapassword'
restore_command = 'cp /var/lib/postgresql/data/pg_wal_archive/%f %p || true'
promote_trigger_file = '/tmp/failover.trigger'
EOSQL

EOF

# Start PostgreSQL под пользователем postgres
echo "Starting PostgreSQL..."
exec gosu postgres postgres -D /var/lib/postgresql/data
