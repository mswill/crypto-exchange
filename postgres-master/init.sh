#!/bin/bash

# Переключение на пользователя postgres для выполнения команд
gosu postgres bash <<EOF

# Создание временного каталога для инициализации
initdb_temp_dir="/var/lib/postgresql/initdb_temp"
mkdir -p \$initdb_temp_dir

# Удаление старого каталога данных, если он существует
rm -rf /var/lib/postgresql/data/*

# Инициализация базы данных во временном каталоге
initdb -D \$initdb_temp_dir

# Копирование файлов из временного каталога в целевой
cp -R \$initdb_temp_dir/* /var/lib/postgresql/data/

# Удаление временного каталога
rm -rf \$initdb_temp_dir

# Добавление записи для репликации в pg_hba.conf
echo "host replication replication 0.0.0.0/0 trust" >> /var/lib/postgresql/data/pg_hba.conf
echo "host all all 0.0.0.0/0 md5" >> /var/lib/postgresql/data/pg_hba.conf

# Установка правильных прав доступа на каталог данных
chown -R postgres:postgres /var/lib/postgresql/data
chmod 700 /var/lib/postgresql/data

# Запуск PostgreSQL
pg_ctl -D /var/lib/postgresql/data -o "-c listen_addresses='*'" -w start

# Создание ролей и базы данных
psql -v ON_ERROR_STOP=1 --username postgres --dbname postgres <<-EOSQL
  CREATE ROLE master WITH LOGIN PASSWORD 'masterpassword';
  ALTER ROLE master CREATEDB;
  CREATE DATABASE master OWNER master;
  CREATE ROLE replication WITH REPLICATION LOGIN PASSWORD 'replicapassword';
  CREATE ROLE replica WITH LOGIN PASSWORD 'replicapassword';
  GRANT CONNECT ON DATABASE master TO replica;
  GRANT USAGE ON SCHEMA public TO replica;
  GRANT SELECT ON ALL TABLES IN SCHEMA public TO replica;
  ALTER DEFAULT PRIVILEGES FOR ROLE master IN SCHEMA public GRANT SELECT ON TABLES TO replica;
EOSQL

# Остановка PostgreSQL
pg_ctl -D /var/lib/postgresql/data -m fast -w stop

# Создание директории для архивации WAL файлов
mkdir -p /var/lib/postgresql/data/pg_wal_archive
chown -R postgres:postgres /var/lib/postgresql/data/pg_wal_archive

# Настройка postgresql.conf
cat >> /var/lib/postgresql/data/postgresql.conf <<-EOSQL
hot_standby = on
listen_addresses = '*'
wal_level = replica
archive_mode = on
archive_command = 'test ! -f /var/lib/postgresql/data/pg_wal_archive/%f && cp %p /var/lib/postgresql/data/pg_wal_archive/%f'
max_wal_senders = 3
wal_keep_size = 16
EOSQL

EOF

# Запуск PostgreSQL под пользователем postgres
exec gosu postgres postgres -D /var/lib/postgresql/data -c config_file=/var/lib/postgresql/data/postgresql.conf
