version: '3.8'

networks:
  crypto-net:

volumes:
  master_data:
  replica_data:
  grafana_data:
  consul_data:

services:
  cms:
    container_name: cms
    build:
      context: ./cms-service
      dockerfile: ./dev.dockerfile
    ports:
      - "11000:11000"
    networks:
      - crypto-net
    environment:
      - KAFKA_ADDR_0=kafka0
      - KAFKA_ADDR_1=kafka1
      - DATABASE_USER=master
      - DATABASE_PASSWORD=masterpassword
      - DATABASE_HOST=postgres-master
      - DATABASE_PORT=5432
      - DATABASE_NAME=master
      - DATABASE_URL=postgres://master:masterpassword@postgres-master:5432/master?sslmode=disable
    command: ["air"]
    volumes:
      - ./cms-service:/app
    depends_on:
      - postgres-master
      - kafka0
      - kafka1
      - consul

  rus:
    container_name: rus
    build:
      context: ./rus-service
      dockerfile: ./dev.dockerfile
    ports:
      - "10000:10000"
    networks:
      - crypto-net
    environment:
      - KAFKA_ADDR_0=kafka0
      - KAFKA_ADDR_1=kafka1
      - DATABASE_USER=master
      - DATABASE_PASSWORD=masterpassword
      - DATABASE_HOST=postgres-master
      - DATABASE_PORT=5432
      - DATABASE_NAME=master
      - FASTFOREX_API_KEY=f91eb928a8-eb9be3cdd5-sesvab
      - FASTFOREX_URL=https://api.fastforex.io
      - DATABASE_URL=postgres://master:masterpassword@postgres-master:5432/master?sslmode=disable
    command: ["air"]
    volumes:
      - ./rus-service:/app
    depends_on:
      - postgres-master
      - kafka0
      - kafka1
      - consul

  api:
    container_name: api
    build:
      context: ./api-service
      dockerfile: ./dev.dockerfile
    ports:
      - "12000:12000"
    networks:
      - crypto-net
    environment:
      - REDIS_ADDR=redis:6379
      - KAFKA_ADDR_0=kafka0:9092
      - KAFKA_ADDR_1=kafka1:9093
      - DATABASE_USER=master
      - DATABASE_PASSWORD=replicapassword
      - DATABASE_HOST=postgres-replica
      - DATABASE_PORT=5432
      - DATABASE_NAME=master
      - DATABASE_URL=postgres://replica:replicapassword@postgres-replica:5432/master?sslmode=disable
    command: ["air"]
    volumes:
      - ./api-service:/app
    depends_on:
      - postgres-master
      - postgres-replica
      - kafka0
      - kafka1
      - redis
      - consul


  postgres-master:
    image: 'postgres:15'
    container_name: postgres-master
    networks:
      - crypto-net
    environment:
      POSTGRES_USER: master
      POSTGRES_PASSWORD: masterpassword
      POSTGRES_DB: master
    volumes:
      - 'master_data:/var/lib/postgresql/data'
      - './postgres-master/postgresql.conf:/etc/postgresql/postgresql.conf'
      - './postgres-master/pg_hba.conf:/etc/postgresql/pg_hba.conf'
      - './postgres-master/init.sh:/docker-entrypoint-initdb.d/init.sh'
    command: bash /docker-entrypoint-initdb.d/init.sh
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U master"]
      interval: 10s
      timeout: 5s
      retries: 5

  postgres-replica:
    image: 'postgres:15'
    container_name: postgres-replica
    networks:
      - crypto-net
    environment:
      POSTGRES_USER: replica
      POSTGRES_PASSWORD: replicapassword
    depends_on:
      - postgres-master
    volumes:
      - 'replica_data:/var/lib/postgresql/data'
      - './postgres-replica/postgresql.conf:/etc/postgresql/postgresql.conf'
      - './postgres-replica/pg_hba.conf:/etc/postgresql/pg_hba.conf'
      - './postgres-replica/init-replica.sh:/docker-entrypoint-initdb.d/init-replica.sh'
    command: bash -c "sleep 15 && bash /docker-entrypoint-initdb.d/init-replica.sh"

  web_admin:
    image: 'dpage/pgadmin4:latest'
    container_name: web_admin
    restart: unless-stopped
    ports:
      - '8000:80'
    expose:
      - 8000
    networks:
      - crypto-net
    environment:
      PGADMIN_DEFAULT_EMAIL: admin@gmail.com
      PGADMIN_DEFAULT_PASSWORD: admin
    depends_on:
      - postgres-master


  redis:
    image: 'redis:6'
    container_name: redis
    ports:
      - "6379:6379"
    networks:
      - crypto-net

  redis-commander:
    image: rediscommander/redis-commander:latest
    container_name: redis-commander
    environment:
      - REDIS_HOSTS=redis:6379
    networks:
      - crypto-net
    ports:
      - "8081:8081"
    depends_on:
      - redis

  grafana:
    image: 'grafana/grafana:latest'
    container_name: grafana
    ports:
      - '3000:3000'
    networks:
      - crypto-net
    environment:
      - GF_SECURITY_ADMIN_PASSWORD=admin
    volumes:
      - 'grafana_data:/var/lib/grafana'
    depends_on:
      - prometheus

  jaeger:
    image: 'jaegertracing/all-in-one:latest'
    container_name: jaeger
    ports:
      - '5775:5775/udp'
      - '6831:6831/udp'
      - '6832:6832/udp'
      - '5778:5778'
      - '16686:16686'
      - '14268:14268'
      - '14250:14250'
      - '9411:9411'
    networks:
      - crypto-net

  prometheus:
    image: 'prom/prometheus:latest'
    container_name: prometheus
    ports:
      - '9090:9090'
    volumes:
      - './prometheus/prometheus.yaml:/etc/prometheus/prometheus.yml'
    networks:
      - crypto-net

  zookeeper:
    image: 'bitnami/zookeeper:latest'
    container_name: zookeeper
    ports:
      - '2181:2181'
    networks:
      - crypto-net
    environment:
      - ALLOW_ANONYMOUS_LOGIN=yes

  kafka0:
    image: 'bitnami/kafka:latest'
    container_name: kafka0
    ports:
      - "9092:9092"
    networks:
      - crypto-net
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_CFG_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP: 'PLAINTEXT:PLAINTEXT'
      KAFKA_CFG_LISTENERS: 'PLAINTEXT://:9092'
      KAFKA_CFG_ADVERTISED_LISTENERS: 'PLAINTEXT://kafka0:9092'
      KAFKA_CFG_AUTO_CREATE_TOPICS_ENABLE: 'true'
      KAFKA_CFG_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
      ALLOW_PLAINTEXT_LISTENER: 'yes'
    depends_on:
      - zookeeper
    healthcheck:
      test: ["CMD", "kafka-topics.sh", "--bootstrap-server=kafka0:9092", "--list"]
      start_period: 15s
      interval: 10s

  kafka1:
    image: 'bitnami/kafka:latest'
    container_name: kafka1
    ports:
      - "9093:9093"
    networks:
      - crypto-net
    environment:
      KAFKA_BROKER_ID: 2
      KAFKA_CFG_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP: 'PLAINTEXT:PLAINTEXT'
      KAFKA_CFG_LISTENERS: 'PLAINTEXT://:9093'
      KAFKA_CFG_ADVERTISED_LISTENERS: 'PLAINTEXT://kafka1:9093'
      KAFKA_CFG_AUTO_CREATE_TOPICS_ENABLE: 'true'
      KAFKA_CFG_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
      ALLOW_PLAINTEXT_LISTENER: 'yes'
    depends_on:
      - zookeeper

  schema-registry:
    image: 'confluentinc/cp-schema-registry:latest'
    container_name: schema-registry
    ports:
      - "8082:8081"
    networks:
      - crypto-net
    environment:
      SCHEMA_REGISTRY_KAFKASTORE_BOOTSTRAP_SERVERS: 'PLAINTEXT://kafka0:9092,PLAINTEXT://kafka1:9093'
      SCHEMA_REGISTRY_HOST_NAME: 'schema-registry'
      SCHEMA_REGISTRY_LISTENERS: 'http://0.0.0.0:8082'
    depends_on:
      - kafka0
      - kafka1

  kafka-ui:
    image: 'provectuslabs/kafka-ui:latest'
    container_name: kafka-ui
    ports:
      - '8080:8080'
    networks:
      - crypto-net
    environment:
      KAFKA_CLUSTERS_0_NAME: 'local'
      KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS: 'kafka0:9092,kafka1:9093'
      KAFKA_CLUSTERS_0_SCHEMAREGISTRY: 'http://schema-registry:8082'
    depends_on:
      - kafka0
      - kafka1
      - schema-registry

  kafka-init:
    image: 'bitnami/kafka:latest'
    container_name: kafka-init
    working_dir: /opt/bitnami/kafka/bin
    entrypoint: /bin/bash
    depends_on:
      kafka0:
        condition: service_started
    networks:
      - crypto-net
    environment:
      KAFKA_CFG_ZOOKEEPER_CONNECT: zookeeper:2181
      TEST_TOPIC_NAME: rates_updated
    volumes:
      - ./rus-service/create_topic.sh:/create_topic.sh
    command: -c "/create_topic.sh"
    init: true

  consul:
    image: consul:1.15.4
    container_name: consul
    restart: always
    volumes:
      - consul_data:/consul/data
    ports:
      - "8500:8500"
      - "8600:8600/tcp"
      - "8600:8600/udp"
    command: "agent -server -ui -node=server-1 -bootstrap-expect=1 -client=0.0.0.0"
    networks:
      - crypto-net
