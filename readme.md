# Crypto Exchange Services

## Сервис обмена валют ( Exchange Service API )

* ### Обрабатывает запросы на конвертацию валют.
* ### Кэширует курсы валют в Redis для быстрого доступа.
* ### Подписывается на сообщения из Kafka для обновления кэша.
* ### Проверяет доступность валют перед выполнением конвертации.
* ### Поддерживает конвертацию как базовых валют в другие, так и наоборот.
* ### Использует валюты в самых наименьших единицах, как и принято в таких проектах.

## Сервис управления валютами ( Currency Management Service )

* ### Управляет доступностью валют.
* ### Добавляет новые валюты.
* ### Удаляет существующие валюты.
* ### Интерфейс для управления валютами (например, REST API).

## Сервис обновления курсов ( Rate Updater Service )

* ### Получает курсы валют с API FastForex.
* ### Обновляет базы данных PostgreSQL.
* ### Отправляет сообщение в Kafka о том, что данные обновлены.

## Адреса сервисов и инструментов

| Сервис                      | Адрес                                                 |
|-----------------------------|-------------------------------------------------------|
| Exchange Service API        | `http://smataruev.fvds.ru:12000/swagger/index.html#/` |
| Currency Management Service | `http://smataruev.fvds.ru:11000/swagger/index.html#/` |
| Grafana                     | `http://smataruev.fvds.ru:3000`                       |
| Jaeger                      | `http://smataruev.fvds.ru:16686`                      |
| Redis Commander             | `http://smataruev.fvds.ru:8081`                       |
| Prometheus                  | `http://smataruev.fvds.ru:9090`                       |
| PGAdmin                     | `http://smataruev.fvds.ru:8000`                       |
| Kafka UI                    | `http://smataruev.fvds.ru:8085`                       |
| Consul                      | `http://smataruev.fvds.ru:8500`                       |

Ниже представлена схема архитектуры нашего приложения.

![Архитектурная схема](./exhange-arch.png)
